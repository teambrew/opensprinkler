// Example code for OpenSprinkler

/* This is a program-based sprinkler schedule algorithm.
 Programs are set similar to calendar schedule.
 Each program specifies the days, stations,
 start time, end time, interval and duration.
 The number of programs you can create are subject to EEPROM size.
 
 Creative Commons Attribution-ShareAlike 3.0 license
 Feb 2013 @ Rayshobby.net
 */

#include <limits.h>
#include <OpenSprinkler.h>
#include "program.h"

// ================================================================================
// This is the path to which external Javascripst are stored
// To create custom Javascripts, you need to make a copy of these scripts
// and put them to your own server, or github, or any available file hosting service

#define JAVASCRIPT_PATH  "http://rayshobby.net/scripts/java/svc1.8.3" 
//"https://github.com/rayshobby/opensprinkler/raw/master/scripts/java/svc1.8.3"
// ================================================================================

// NTP sync interval (in seconds)
#define NTP_SYNC_INTERVAL       86400L  // 24 hours default
// RC sync interval (in seconds)
#define RTC_SYNC_INTERVAL       60     // 1 minute default
// Interval for checking network connection (in seconds)
#define CHECK_NETWORK_INTERVAL  60     // 1 minute default
// Ping test time out (in milliseconds)
#define PING_TIMEOUT            200     // 0.2 second default


// ====== Ethernet defines ======
byte mymac[] = { 0x00,0x69,0x69,0x2D,0x30,0x00 }; // mac address: the last byte will be substituted by device id
byte ntpip[] = {204,9,54,119};                    // Default NTP server ip
uint8_t ntpclientportL = 123;                           // Default NTP client port
int myport;
static bool bToggle = false;
static bool bEnable = false;
static int iDelay = 100;
char mystring[50];
 
byte Ethernet::buffer[ETHER_BUFFER_SIZE]; // Ethernet packet buffer
char tmp_buffer[TMP_BUFFER_SIZE+1];       // scratch buffer
BufferFiller bfill;                       // buffer filler

// ====== Object defines ======
OpenSprinkler svc;    // OpenSprinkler object
ProgramData pd;       // ProgramdData object 

// ====== UI defines ======
static char ui_anim_chars[3] = {'.', 'o', 'O'};
  
// poll button press
void button_poll() {
  
  
  // read button, if something is pressed, wait till release
  byte button = svc.button_read(BUTTON_WAIT_HOLD);

  if (!(button & BUTTON_FLAG_DOWN)) return;  // repond only to button down events

  switch (button & BUTTON_MASK) {
  case BUTTON_1:
    if (button & BUTTON_FLAG_HOLD) {
      // hold button 1 -> start operation
      //svc.enable();
	  iDelay ++;
	  sprintf(mystring,"Disabled : SetPoint = %i",iDelay);
    } 
    else {
      // click button 1 -> display ip address and port number
      //svc.lcd_print_ip(ether.myip, ether.hisport);
      //delay(DISPLAY_MSG_MS);
	  bEnable = true;
	  sprintf(mystring,"Enabled : SetPoint = %i",iDelay);
	  
	  
    }
	svc.lcd_print_line_clear_pgm(mystring, 1);
    break;

  case BUTTON_2:
    if (button & BUTTON_FLAG_HOLD) {
      // hold button 2 -> disable operation
      //svc.disable();
	  iDelay --;
	  sprintf(mystring,"Disabled : SetPoint = %i",iDelay);
    } 
    else {
      // click button 2 -> display gateway ip address and port number
      //svc.lcd_print_ip(ether.gwip, 0);
      //delay(DISPLAY_MSG_MS);
	  bEnable = false;
	  sprintf(mystring,"Disabled : SetPoint = %i",iDelay);
	  
    }
	svc.lcd_print_line_clear_pgm(mystring, 1);
    break;

  case BUTTON_3:
    if (button & BUTTON_FLAG_HOLD) {
      // hold button 3 -> reboot
      //svc.button_read(BUTTON_WAIT_RELEASE);
      //svc.reboot();
	  
    } 
    else {
      // click button 3 -> switch board display (cycle through master and all extension boards)
      	if (bToggle == true) {
			change_station(0);
			bToggle = false;
			if (bEnable)
				{
					sprintf(mystring,"Enabled : Off: SetPoint = %i",iDelay);
					
				}
				else
				{
					sprintf(mystring,"Disabled : Off: SetPoint = %i",iDelay);
					
				}
			
			}
		else {
			change_station(1);
			bToggle = true;
			if (bEnable)
				{
					sprintf(mystring,"Enabled : On: SetPoint = %i",iDelay);
					
				}
				else
				{
					sprintf(mystring,"Disabled : On: SetPoint = %i",iDelay);
					
				}
		}
		
	  svc.lcd_print_line_clear_pgm(mystring, 1);
	  svc.status.display_board = (svc.status.display_board + 1) % (svc.nboards);
    }
    break;
  }
}

// ======================
// Arduino Setup Function
// ======================
void setup() { 

  svc.begin();          // OpenSprinkler init
  svc.options_setup();  // Setup options
  pd.init();            // ProgramData init
  // calculate http port number
  myport = (int)(svc.options[OPTION_HTTPPORT_1].value<<8) + (int)svc.options[OPTION_HTTPPORT_0].value;

  svc.lcd_print_line_clear_pgm(PSTR("Connecting to"), 0);
  svc.lcd_print_line_clear_pgm(PSTR(" the network..."), 1);  
    
  if (svc.start_network(mymac, myport)) {  // initialize network
    svc.status.network_fails = 0;
  } else  svc.status.network_fails = 1;

  //delay(500);
  
  setSyncInterval(RTC_SYNC_INTERVAL);  // RTC sync interval: 15 minutes

  // if rtc exists, sets it as time sync source
  setSyncProvider(svc.status.has_rtc ? RTC.get : NULL);

  svc.apply_all_station_bits(); // reset station bits
  
  perform_ntp_sync(now());
  
  svc.lcd_print_time(0);  // display time to LCD

  //wdt_enable(WDTO_4S);  // enabled watchdog timer
}

// =================
// Arduino Main Loop
// =================
void loop()
{
  static long last_time = 0;
  static unsigned long last_minute = 0;
  static uint16_t pos;
  static long curr_time = 0;
    
  byte bid, sid, s, pid, seq, bitvalue, mas;
  ProgramStruct prog;

	seq = svc.options[OPTION_SEQUENTIAL].value;
  mas = svc.options[OPTION_MASTER_STATION].value;
  //wdt_reset();  // reset watchdog timer

  // ====== Process Ethernet packets ======
  int plen=0;
  pos=ether.packetLoop(ether.packetReceive());
  if (pos>0) {  // packet received
    bfill = ether.tcpOffset();
    analyze_get_url((char*)Ethernet::buffer+pos);

    ether.httpServerReply(bfill.position());   
  }
  // ======================================
 
  button_poll();    // process button press


  // if 1 second has passed
//  time_t curr_time = now();

curr_time = millis();

  if ((last_time - curr_time < 0) && bEnable) 
  {

    last_time = curr_time + iDelay; // 400ms between "cycles" or 2.5 times/sec
    svc.lcd_print_time(0);       // print time
    
	if (bToggle == true) {
		change_station(0);
		bToggle = false;
		//svc.lcd_print_line_clear_pgm(PSTR("On:Off"), 1);
		}
	else {
		change_station(1);
		bToggle = true;
		//svc.lcd_print_line_clear_pgm(PSTR("On:On"), 1);
		}
      // activate/deactivate valves
    
    // activate/deactivate valves
    svc.apply_all_station_bits();
    
    // process LCD display
    //svc.lcd_print_station(1, ui_anim_chars[curr_time%3]);
    
    // check network connection
    //check_network(curr_time);
    
    // perform ntp sync
    //perform_ntp_sync(curr_time);
  }
}

void change_station (bool state)
{
	for (int i = 1;i<= 8;i++)
	{
		svc.set_station_bit(i,state);
	}
}


void manual_station_off(byte sid) {
  unsigned long curr_time = now();

  // set station stop time (now)
  pd.scheduled_stop_time[sid] = curr_time;  
}

void manual_station_on(byte sid, int ontimer) {
  unsigned long curr_time = now();
  // set station start time (now)
  pd.scheduled_start_time[sid] = curr_time + 1;
  if (ontimer == 0) {
    pd.scheduled_stop_time[sid] = ULONG_MAX-1;
  } else { 
    pd.scheduled_stop_time[sid] = pd.scheduled_start_time[sid] + ontimer;
  }
  // set program index
  pd.scheduled_program_index[sid] = 99;
  svc.status.program_busy = 1;
}

void perform_ntp_sync(time_t curr_time) {
  static unsigned long last_sync_time = 0;
  // do not perform sync if this option is disabled, or if network is not available
  if (svc.options[OPTION_USE_NTP].value==0 || svc.status.network_fails>0) return;   
  // sync every 1 hour
  if (last_sync_time == 0 || (curr_time - last_sync_time > NTP_SYNC_INTERVAL)) {
    last_sync_time = curr_time;
    unsigned long t = getNtpTime();   
    if (t>0) {    
      setTime(t);
      if (svc.status.has_rtc) RTC.set(t); // if rtc exists, update rtc
    }
  }
}

void check_network(time_t curr_time) {
  static unsigned long last_check_time = 0;

  if (last_check_time == 0) {last_check_time = curr_time; return;}
  // check network condition periodically
  if (curr_time - last_check_time > CHECK_NETWORK_INTERVAL) {
    last_check_time = curr_time;
   
    // ping gateway ip
    ether.clientIcmpRequest(ether.gwip);
    
    unsigned long start = millis();
    boolean failed = true;
    // wait at most PING_TIMEOUT milliseconds for ping result
    do {
      ether.packetLoop(ether.packetReceive());
      if (ether.packetLoopIcmpCheckReply(ether.gwip)) {
        failed = false;
        break;
      }
    } while(millis() - start < PING_TIMEOUT);
    if (failed)  svc.status.network_fails++;
    else svc.status.network_fails=0;
    // if failed more than 2 times in a row, reconnect
    if (svc.status.network_fails>2&&svc.options[OPTION_NETFAIL_RECONNECT].value) {
      //svc.lcd_print_line_clear_pgm(PSTR("Reconnecting..."),0);
      svc.start_network(mymac, myport);
      //svc.status.network_fails=0;
    }
  } 
}

void schedule_all_stations(unsigned long curr_time, byte seq)
{
  unsigned long accumulate_time = curr_time + 1;
  byte sid;
  // calculate start time of each station
	if (seq) {
		// in sequential mode
	  // stations run one after another
  	// separated by station delay time

		for(sid=0;sid<svc.nstations;sid++) {
		  if(pd.scheduled_stop_time[sid]) {
		    pd.scheduled_start_time[sid] = accumulate_time;
		    accumulate_time += pd.scheduled_stop_time[sid];
		    pd.scheduled_stop_time[sid] = accumulate_time;
		    accumulate_time += svc.options[OPTION_STATION_DELAY_TIME].value; // add station delay time
		    svc.status.program_busy = 1;  // set program busy bit
		  }
		}
	} else {
		// in concurrent mode, stations are allowed to run in parallel
    for(sid=0;sid<svc.nstations;sid++) {
			byte bid=sid/8;
			byte s=sid%8;
      if(pd.scheduled_stop_time[sid] && !(svc.station_bits[bid]&(1<<s))) {
        pd.scheduled_start_time[sid] = accumulate_time;
        pd.scheduled_stop_time[sid] = accumulate_time + pd.scheduled_stop_time[sid];
        svc.status.program_busy = 1;  // set program busy bit
      }
    }
	}
}

void reset_all_stations() {
  svc.clear_all_station_bits();
  svc.apply_all_station_bits();
  pd.reset_runtime();
}
